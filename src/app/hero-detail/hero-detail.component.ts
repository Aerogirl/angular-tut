import { Component, OnInit, Input } from '@angular/core';
import { Hero } from '../hero';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {

	@Input() hero1: Hero;
	@Input() hero: Hero;

	changed: string;

  constructor() { }

  ngOnInit() {

  		this.changed = 'Imię:' + this.hero["name"];
  }

}
